v0.0.1


**I archived this GitHub repo as I have moved it to [GitLab](https://gitlab.com/tukusejssirs/skdict).**

# Komplexný slovenský slovník (nielen) pre kancelárske balíky
Tieto súbory obsahujú slová, ktoré sa používajú v slovenskom jazyku.

Slovníky sú rozdelené podľa slov, ktoré obsahujú dané súbory:
[1] spisovné slová
[2] nespisovné slová (nárečové, slangové, hovorové)
[3] priezviská (aj so všetkými ich spisovnými vyskloňovanými tvarmi) – tento určite nie je a ani nikdy nebude úplne kompletný


sk_sk_firmy_a_pravnicke_subjekty.dic
sk_sk_krajiny_staty_obce_narodnosti_zahranicne.dic  # kontinenty, krajiny, staty, narodnosti, mesta, obce, dediny, lazy sveta okrem Slovenska - len realne, ci z minulosti, ci aktualne, ci starsie nazvy, vsetky tvary (sg/pl, 1. az 7. pad; celkovo 14 padov pre obyvatelov; pre krajiny/staty 7 padov)
sk_sk_krstne_mena.dic  # slovenske i zahranicne krstne mena (vratane zdrobnenin, zdomacnenych foriem ci mien bohov)  vo vsetkych 14 padoch
sk_sk_nespisovne_slova.dic  # narecove, slangove a hovorove
sk_sk_obce_obyvatelia_sk.dic  # mesta, obce, dediny, lazy na Slovensku a mena ich obyvatelov
sk_sk_priezviska.dic  # slovenske i zahranicne priezviska, prip. rody (Anjouovci, Habsburský rod, atd) ( nikdy nebude kompletny)
sk_sk_skratky.dic  # skratky a znacky
sk_sk_spisovne_slova.dic  # vsetky spisovne slova okrem slov uvedenych vo vyssie uvedenych slovnikoch
sk_sk_komplet.dic  # vsetky slova obsiahnute vo vyssie uvedenych slovnikoch

## Vseobecne platiace pravidla
[1] slova vramci kazdeho jedneho slovnika su usporiadane abecedne podla slovenskej abecedy
[2] slovniky neobsahuju ziadne duplicity (ani vramci jedneho slovnika, ani vramci 2 roznych slovnikov s vynimkou sk_sk_komplet.dic, ktory obsahuje vsetky slova z predchadzajuch slovnikov)
[3] sk_sk_komplet.dic nikdy nie je editovany manualne, ale 'generovany' (spoja sa vsetkych ostatne slovniky, zoradia sa vzostupne podla slovenskej abecedy)


## Slovníky treba pred ich použitím v tom-ktorom programe/aplikácii upraviť tak, aby ich formát bol akceptovaný tým-ktorým programom/aplikáciou!
### MS Office
### LibreOffice / OpenOffice
### GBoard (Google Klávesnica)
### Hacker's Keyboard


## Bez dovolenia som pouzil nasledujuci subor, ktory je zakladom tychto slovnikov a ktory je vlastnictvom JÚĽŠ, SAV v Bratislave
http://korpus.sk/attachments/morphology_database/ma-2015-02-05.txt.xz

## Známe chyby (Known Issues)

## TODO / osnova
[ ] roztriedit hlavny zdrojovy subor (ma-2015-02-05.txt → sk_sk_spisovne_slova.dic) - kym nedokoncim triedenie, dvojentrom (\n\n) si znacim, kde som skoncil)
[ ] rozhodnut sa, co s homonymami, ktore patria do roznych slovnikov (napr Bakša ako obec a ako priezvisko - v tomto pripade sa dokonca i inac sklonuju), prip. aj po vysklonovani danych slov (o Beloch - dvaja muzi s menom Belo - a beloch ako clen bielej rasy)
[ ] skontrolovat, ci su vsetky tvary vsetkych slov (zacat krstnymi menami, priezviskami, sk i zahranicnymi obcami/krajinami/kontinentmi a ich obyvatelmi) ... nezabudnut aj na adjektiva od danych osob (teda 14 padov pods. mien - okrem pomnoznych, ktore maju len 7 padov -, 14 padov adjektiv; niektore slova maju dvortvary ako lucensky/lucenecky, novomestsky/vazskonovomestsky)
